package com.question;

public class Question4 extends Thread{
public static boolean flag = false;
	
	static {
		Question4 thread = new Question4();
		thread.setDaemon(true);
		thread.start();
	}
	public void run() {		
		if(Thread.currentThread().isDaemon())
        {
			flag =true;
            System.out.println(getName() + " is Daemon thread");
        }
          
        else
        {
        	flag = false;
            System.out.println(getName() + " is User thread");
        }
	}

}
