package com.question;

public class Main {

	public static void main(String[] args) {

		Question4 thread1 = new Question4();
		Question4 thread2 = new Question4();
		Question4 thread3 = new Question4();

		thread1.setDaemon(true);
		
		thread1.start();
		thread2.start();
		
		thread3.setDaemon(true);
		thread3.start();
	}
}
