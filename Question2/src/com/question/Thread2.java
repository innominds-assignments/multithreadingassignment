package com.question;

public class Thread2 extends Thread {
	int count = 1;
	int n = 10;

	synchronized public void run(int n) {
		
		System.out.println("---pong");
		printEvenNumber();
	}

	public void  printEvenNumber() {
		while (count < n) {
			while(count%2==1) {
				try {
					wait();
					//Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println(count + " ");
				count++;
				notify();
			}

		}

	}
}
