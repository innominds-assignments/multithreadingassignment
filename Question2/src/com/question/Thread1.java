package com.question;

public class Thread1 extends Thread {

	int count = 1;
	int n = 10;

	synchronized public void run(int n) {
		System.out.println("ping--->");
		printOddNumber();
		
	}
	
	public void printOddNumber() {
		while (count < n) {
			while (count % 2 == 0) {
				try {
					wait();
					//Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println(count + " ");
				count++;
				notify();
			}
		}
	}
}
