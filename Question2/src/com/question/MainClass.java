package com.question;

import java.util.Scanner;

public class MainClass {
	static int count=1;
	
	public static void main(String[] args) {
		int n = 10;
		
		long sleep1 = Long.parseLong(args[0]);
		long sleep2 = Long.parseLong(args[1]);

		//ThreadUtils tu = new ThreadUtils();

		Thread1 t1 = new Thread1();
		Thread2 t2 = new Thread2();
		
		t1.start();
		t2.start();
		
		/*
		 * Thread t1 = new Thread(new Runnable() {
		 * 
		 * @Override synchronized public void run() {
		 * 
		 * while(count<n) { while(count % 2 == 0 ) { try { //Thread.sleep(2000);
		 * wait(1000); }catch(InterruptedException e) { e.printStackTrace(); }
		 * System.out.println(count + " "); count++; notify(); } }
		 * System.out.println("ping --->");
		 * 
		 * } });
		 * 
		 * Thread t2 = new Thread(new Runnable() {
		 * 
		 * @Override synchronized public void run() {
		 * 
		 * while(count<n) { while(count%2==1) { try { wait(1000); //Thread.sleep(1000);
		 * } catch (InterruptedException e) { e.printStackTrace(); }
		 * System.out.println(count + " "); count++; notify(); } }
		 * System.out.println("---pong");
		 * 
		 * } });
		 * 
		 * t1.start(); t2.start();
		 */
	}
}
