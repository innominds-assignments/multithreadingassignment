package com.question;

public class ThreadUtils {
	
	int count = 1;
 static	int n;
	

	
	
	public void printOddNumber() {
		synchronized (this) {
			while(count<n) {
				while(count % 2 == 0 ) {
					try {
						Thread.sleep(1000);
					}catch(InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println(count + " ");
					count++;
					notify();
				}
			}
		}
		
		
	}

	public void printEvenNumbers() {
		synchronized(this) {
			while(count<n) {
				while(count%2==1) {
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println(count + " ");
					count++;
					notify();
				}
			}
		}
	}
}
