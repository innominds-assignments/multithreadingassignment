package com.question;

import java.util.LinkedList;

//Java Program to demonstrate Producer Consumer using multithreading

public class ProducerConsumer {

	LinkedList<Integer> list = new LinkedList<>();
	int capacity = 2;
	
	 public void produce() throws InterruptedException
     {
		 int value = 0;
		 
		 while(true) {
			 synchronized (this) {
				while(list.size()==capacity) {
					wait();
				}
				System.out.println("Producer is producing " + value);
				
				list.add(value++);
				
				notify();
				
				Thread.sleep(1000);
			}
		 }
     }
		 
		 public void consume() throws InterruptedException
	        {
			 while(true) {
				 synchronized(this) {
					 while(list.size()==0) {
						 wait();}
					 
					 int val = list.removeFirst();
					 
					 System.out.println("Consumer is consuming : " + val);
					 
					 notify();
					 
					 Thread.sleep(1000);
				 }
			 
	        }
     }
     
}
