package com.question;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MainApp {
	public static void main(String[] args) {
		Book newBook = new Book("Java", "300", "VS", "5A");
		System.out.println("Before serialization " +newBook);
	
	
		try {
			FileOutputStream stream1 = new FileOutputStream("newBook.ser");
			
			ObjectOutputStream str = new ObjectOutputStream(stream1);
			str.writeObject(newBook);
			
			stream1.close();
			str.close();
			
			FileInputStream inputStream = new FileInputStream("newBook.ser");
			ObjectInputStream str2 = new ObjectInputStream(inputStream);
			
			Book book = (Book) str2.readObject();
			System.out.println("book after serialization : " +book);
			inputStream.close();
			str2.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
