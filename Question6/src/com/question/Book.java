package com.question;

import java.io.Serializable;

public class Book implements Serializable {

	private String title;
	private String price;
	private String author;
	private transient String edition ;
	
	public Book(String title, String price, String author, String edition) {
		super();
		this.title = title;
		this.price = price;
		this.author = author;
		this.edition = edition;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	@Override
	public String toString() {
		return "Book [title=" + title + ", price=" + price + ", author=" + author + ", edition=" + edition + "]";
	}
	
	
	
}
