package com.question.usingRunnable;

import com.question.GoodMorning;

public class Main {
	public static void main(String[] args) {
		
		Thread t1 = new Thread(new GoodMorning());
		Thread t2 = new Thread(new Hello());
		Thread t3 = new Thread(new Welcome());
	
		t1.start();
		t2.start();
		t3.start();
	
	}
}
