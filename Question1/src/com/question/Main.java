package com.question;

public class Main {
	public static void main(String[] args) {
		
		GoodMorning gm = new GoodMorning();
		Hello h = new Hello();
		Welcome wc = new Welcome();
		
		gm.start();
		h.start();
		wc.start();
		
	}
}
