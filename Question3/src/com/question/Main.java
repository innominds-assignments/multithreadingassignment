package com.question;

public class Main {

	public static void threadCreation() {
		QuestionThread t1 = new QuestionThread();
		QuestionThread t2 = new QuestionThread();
		QuestionThread t3 = new QuestionThread();
		QuestionThread t4 = new QuestionThread();
		QuestionThread t5 = new QuestionThread();
	
		t1.setPriority(10);
		t2.setPriority(8);
		t3.setPriority(6);
		t4.setPriority(5);
		t5.setPriority(4);
		
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
	}
	
	
	public static void main(String[] args) throws InterruptedException {
		
		threadCreation();
		Thread.sleep(5000);
		System.out.println("Long Lasting Thread with priority " + QuestionThread.flag);
	}
}
